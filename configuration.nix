# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:


let
  unstableTarball =
    fetchTarball https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz;
in

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  nixpkgs.config = {
    packageOverrides = pkgs: {
      unstable = import unstableTarball {
        config = config.nixpkgs.config;
      };
    };
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Kyiv";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "uk_UA.UTF-8";
    LC_IDENTIFICATION = "uk_UA.UTF-8";
    LC_MEASUREMENT = "uk_UA.UTF-8";
    LC_MONETARY = "uk_UA.UTF-8";
    LC_NAME = "uk_UA.UTF-8";
    LC_NUMERIC = "uk_UA.UTF-8";
    LC_PAPER = "uk_UA.UTF-8";
    LC_TELEPHONE = "uk_UA.UTF-8";
    LC_TIME = "uk_UA.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager = {
    autoLogin.enable = true;
    autoLogin.user = "me";
    gdm.enable = true;
  };
  services.xserver.desktopManager.gnome.enable = true;


#  services.xserver.displayManager.defaultSession = "none+awesome";
#  services.xserver.displayManager = {
#    autoLogin.enable = true;
#    autoLogin.user = "me";
#    gdm.enable = true;
#  };
#  services.xserver.windowManager.awesome = {
#    enable = true;
#    luaModules = with pkgs.luaPackages; [
#      luarocks       # lua package manager
#      luadbi-mysql   # database abstracton layer
#    ];
#  };


  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # LD Fix
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    xorg.libX11
    xorg.libXrender
    xorg.libXxf86vm
    xorg.libXfixes
    xorg.libXi
    xorg.libSM
    xorg.libICE
    xorg.libXext
    libGL
    libxkbcommon
#    libstdcxx5
    stdenv.cc.cc.lib
    libz
  ];

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  security.rtkit.enable = true;
  hardware.pulseaudio.enable = false;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.me = {
    isNormalUser = true;
    description = "me";
    extraGroups = [
	"networkmanager"
	"wheel"
  "audio"
  "realtime"
    ];
    packages = with pkgs; [
    #  thunderbird
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;
  programs.steam.enable = true;
  environment.systemPackages = with pkgs; [
    (vim_configurable.customize {
      name = "vim";
      vimrcConfig.packages.myplugins = with pkgs.vimPlugins; {
        start = [ vim-nix ];
        opt = [];
      };
      vimrcConfig.customRC = ''
        :syn on         " Syntax highlighting
        :set ruler
        set nocompatible
        ":set spell      " Spell checking
        set showmatch   " When a bracket is inserted, briefly jump to a matching one
        set incsearch   " Incremental search
        set backspace=indent,eol,start

        set clipboard=unnamedplus

        " nix syntax
        "au BufRead,BufNewFile *.nix set filetype=nix

        " --- Tab settings ---
        set tabstop=2
        set shiftwidth=2
        " Expand tabs for Python coding only (C/C++ in Blender uses tabs)
        " set expandtab
        set smarttab

        " ---- Indenting ----
        set autoindent    " Auto indent
        set smartindent   " Smart indent
        set ci            " C/C++ indents
        set cin

        " --- Column/Row Stuff ---
        "set cul                     " Highlight the current line
        :set number relativenumber   " Show line numbers
        ":set nu rnu
        ":set lines=40 columns=120    " Window size
        :set colorcolumn=120

        " --- Extra functionality helpers ---
        filetype plugin on
        filetype indent on
        filetype plugin indent on

        " haskell-vim
        " https://github.com/neovimhaskell/haskell-vim
        let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
        let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
        let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
        let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
        let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
        let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
        let g:haskell_backpack = 1                " to enable highlighting of backpack keywords

        " auto-complete
        set ofu=syntaxcomplete#Complete

        " --- Macros ---
        let @c = 'I#'
        let @u = '0xj'
      '';
    })

    git
    ranger

    kitty
    firefox
    alsaUtils
    pavucontrol

    #''
    (pkgs.haskellPackages.ghcWithPackages (ghcpkgs: with ghcpkgs; [
      apecs  # Fast Entity-Component-System library for game programming
      brick  # a declarative terminal user interface library
#      fwgl   # a library for interactive 2D and 3D applications and games
      gloss  # painless 2D vector graphics, animations and simulations
#      helm   # a functionally reactive game engine
      random
      split
      vty    # terminal GUI library in the niche of ncurses
    ]))
    
    gnome.gnome-tweaks
    gnome.dconf-editor
    gnomeExtensions.no-overview
    gnomeExtensions.vim-alt-tab
    gnomeExtensions.one-window-wonderland
    gnomeExtensions.vitals
    unstable.gnomeExtensions.undecorate-window-for-wayland
    tracker
    libsForQt5.qtstyleplugin-kvantum
    qt5ct
    devilspie2

    appimage-run
    betaflight-configurator
    edgetx

    acpi
    bat
    cmus
    curl
#    dwarf-fortress
    ffmpeg
    file
    gcc
    gotop
    hlint
    htop
    iftop
#    jmtpfs
    killall
    lm_sensors
    lsof
    milkytracker
    mlocate
    nmap
    neofetch
    nethack
    pciutils
    p7zip
    unstable.ponysay
    python3
#    rustc rustup cargo
#    testdisk
    texlive.combined.scheme-full
    tldr
    tree
    wget
    which
    xclip
    yt-dlp
#    unixtools.xxd
    w3m
    zathura
##    zathura-with-plugins

    steam
    unstable.steam-run

#    ardour
    unstable.bespokesynth #surge-XT
    unstable.blender
    brave
    cool-retro-term
    dosbox
    godot_4
    inkscape
#    lightlocker
    mixxx
    mpv
    krita
    libreoffice
#    obs-studio
    qbittorrent
    qutebrowser
#    renderdoc   # A single-frame graphics debugger
    solvespace
    sxiv
    xorg.xkill
#    xscreensaver
  
    (retroarch.override {
      cores = with libretro; [
        nestopia         # nes
        snes9x           # snes
        genesis-plus-gx  # megadrive / genesis
        beetle-gba       # ngba
        beetle-psx       # psx
        beetle-psx-hw    # psx
        pcsx-rearmed     # psx
        ppsspp           # psp
        mame             # arcade
      ];
    })

    unstable.flameshot #gsettings-desktop-schemas

    renoise
    supercollider
    sfxr-qt
#    qjackctl
#    cadence


#    resilio-sync   # Automatically sync files via secure, distributed technology
#    openmvg   # A library for computer-vision scientists and targeted for the Multiple View Geometry community

    #''
  ];

  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = ["user-with-access-to-virtualbox"];


  # Fonts
  fonts.fontDir.enable = true;
  fonts.packages = with pkgs; [
    mononoki
    inconsolata-nerdfont
    ricty  # A high-quality Japanese font based on Inconsolata and Migu 1M
  ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

#  services.picom = {
#    enable = true;
#    backend = "xrender";
#    vSync = true;
#    settings = {
#      shadow = true;
#      shadow-radius   =  55;
#      sahdow-offset-x = -55;
#      shadow-offset-y = -55;
#      shadow-opacity  = 0.8;
#      
#      inactive-dim = 0.2;
#      inactive-dim-fixed = true;
#    };
#  };

  environment.variables = {
    "QT_STYLE_OVERRIDE"="kvantum";
  };

#  services.unclutter-xfixes = {
#    enable = true;
#    timeout = 1;
#    threshold =1;
#  };

  services.xbanish = {
    enable = true;
    arguments = "-t1 -s";
  };


  services.flatpak.enable = true;
  services.emacs.enable = true;
  services.emacs.package = with pkgs; (emacsWithPackages (with emacs.pkgs; [
    smex
    nix-mode
    haskell-mode
    python-mode
    lua-mode
    multiple-cursors
    magit
    hindent
    sclang-extensions
  ]));

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
